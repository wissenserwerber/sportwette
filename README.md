# Sportwette Anwendung

## Eine Web Scraping Anwendung die die Daten aus der Sportwetten Seiten herauszieht und sie in eine ausführliche und dynamische Tabelle erfasst.

### Die verwendeten Sprachen: Python, Javascript, HTML
### Die Anwendungsgerüste: Selenium
### Datenbank: JSON

### Voraussetzungen: Das Selenium Python-Modul soll installiert sein. 

Diese Anwendung wurde für einen Kunden Dezember 2016 geschrieben. Sie zieht die Home und Away Werte der Spiele heraus, die auf der ganzen Welt gespielt werden. Sie zieht auch die Over und Under Werte für jedes herausgezogenes Spiel heraus, aber der betroffene Code funktionerte nicht mehr weil die Sportwette Website ihre HTML Codes ein wenig verändert hat.

![Demo](Sportwette-Bildschirm-Aufnahme.mp4)