#!C:\Python27\python.exe
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from shutil import copyfile
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import sys
import psutil
import time
import json

from datetime import datetime
from pytz import timezone
import pytz

import ConfigParser
# ###################################################### #

# def json2table(arg01,arg02,arg03)
# def oufinder(arg01, arg02, arg03):

reload(sys)
sys.setdefaultencoding("utf8")

# ##################################### #
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
import sys
import psutil
from urlparse import urlparse
import time
# ###################################### #

# ###################################### #
import json
from datetime import datetime
from shutil import copyfile
from sys import argv
# ###################################### #

# ################################ #
def prockiller(firefox_gecko_list_arg):
    for proc in psutil.process_iter():
        try:
            pinfo = proc.as_dict(attrs=['pid', 'name'])
        except psutil.NoSuchProcess:
            print "no such proc"
            pass
        else:
            if proc.name() == 'geckodriver.exe':
                proc.kill()
            if proc.name() == 'firefox.exe':
                if proc.pid in firefox_gecko_list_arg:
                    proc.kill()
# ###################################### #
fmt='%d.%m.%Y %H:%M'
# ############################################# #
def bogo_bubble(blist):
    cmpcount, swapcount = 0, 0
    n = 0
    while n < len(blist) - 1:
        StartDate1 = datetime.strptime(blist[n]["StartDate"], fmt)
        StartDate2 = datetime.strptime(blist[n+1]["StartDate"], fmt)
        cmpcount += 1
        if StartDate1 > StartDate2:
            swapcount += 1
            swap = blist[n]
            blist[n] = blist[n+1]
            blist[n+1] = swap
            n = 0
        else:
            n += 1
    return blist, cmpcount, swapcount
# ############################################# #

def json2table(arg01,arg02,arg03):

    fmt='%Y%m%d%H%M%S'
    bgColor="Yellow"
    fgColor="Black"

    dosyaismi_json_arg = "sportwette_over_under.json" # OU -> Over/Under
    dosyaismi_sonuc_arg = "bets_sonuc.htm"
    sirala = 0

    dosyaismi_json = dosyaismi_json_arg
    dosyaismi_sonuc = dosyaismi_sonuc_arg
    siralansin_mi = sirala

    dosya_dictlist = open(dosyaismi_json, 'r')
# ########### CONFIG
    config = ConfigParser.ConfigParser()
    filename = "konfig.txt"
    config.read(filename)
# ###########    
    try:
        dictlist = json.load(dosya_dictlist) # load-->object loads-->String
    except Exception:
        print "error: " + dosyaismi_json

    dosya_dictlist.close()
    # #####################


    # #####################
    dosya_htmlhead= open('html-head.htm', 'r')
    htmlhead = dosya_htmlhead.read()
    dosya_htmlhead.close()

    dosya_sonuc = open(dosyaismi_sonuc, 'w')

    print >>dosya_sonuc, htmlhead
    print >>dosya_sonuc, '<body>'
    
    print >>dosya_sonuc, '<div class="container">'
    print >>dosya_sonuc, '<div class="info"><b>HA Listesi - Son OU Cekim Baslangic:</b> ' + config.get('Einstellungen', 'SonOU') + ' <b>HA=</b> ' + config.get('Einstellungen', 'ha_seite_steuck') + ' <b>OU=</b> ' + config.get('Einstellungen', 'ouisleneceksayfaadet') + '<br /></div>'
    ana=1
    for giris in dictlist:
        alt=1
        giris_id=giris["Id"]
        print >>dosya_sonuc, '<div class="event">'
        print >>dosya_sonuc, '<div class="event-left">'
        print >>dosya_sonuc, '<div class="event-row">' + str(ana).encode('utf-8') + '- <a target="__blank" href="' + giris['Link'] + '">' + giris['StartDate'] + '</a></div>'
        baslik_liste = giris['Name'].split(u'-')
        print >>dosya_sonuc, '<div class="event-row">' + baslik_liste[0].strip().encode('utf-8') + '</div>'
        print >>dosya_sonuc, '<div class="event-row">' + baslik_liste[1].strip().encode('utf-8') + '</div>'
        print >>dosya_sonuc, '</div>'

        print >>dosya_sonuc, '<div class="event-right">'
        home_tmp=0
        away_tmp=0
        for tahmin in giris['Odds']:

            tarih_saat=tahmin['Date'][-5:]
            name = giris_id+"-"+tarih_saat

            print >>dosya_sonuc, """<div class="event-column"><a onMouseOver="eShow('""" + name + """')" onMouseOut="eHide('""" + name + """')">"""

            print >>dosya_sonuc, '<div class="event-row">' + tarih_saat + '</div>'

            home_current = float(tahmin['Home']) if tahmin['Home'] != "NA" else "NA"
            away_current = float(tahmin['Away']) if tahmin['Away'] != "NA" else "NA"

            if home_current != "NA":
                if home_tmp == 0 or home_current == home_tmp:
                    print >>dosya_sonuc, '<div class="event-row">' + tahmin['Home'] + '<i class="fa fa-minus"></i></div>'
                elif home_current > home_tmp:
                    print >>dosya_sonuc, '<div class="event-row">' + tahmin['Home'] + '<i class="fa fa-arrow-up"></i></div>'
                elif home_current < home_tmp:
                    print >>dosya_sonuc, '<div class="event-row">' + tahmin['Home'] + '<i class="fa fa-arrow-down"></i></div>'
            else:
                print >>dosya_sonuc, '<div class="event-row">' + 'NA' + '<i class="fa fa-minus"></i></div>'

            if away_current != "NA":
                if away_tmp == 0 or away_current == away_tmp:
                    print >>dosya_sonuc, '<div class="event-row">' + tahmin['Away'] + '<i class="fa fa-minus"></i></div>'
                elif away_current > away_tmp:
                    print >>dosya_sonuc, '<div class="event-row">' + tahmin['Away'] + '<i class="fa fa-arrow-up"></i></div>'
                elif away_current < away_tmp:
                    print >>dosya_sonuc, '<div class="event-row">' + tahmin['Away'] + '<i class="fa fa-arrow-down"></i></div>'
            else:
                print >>dosya_sonuc, '<div class="event-row">' + 'NA' + '<i class="fa fa-minus"></i></div>'

            ########################### OU #############################

            html_tbl = '<div style="position:relative">'

            html_tbl += '<div style="position:relative;color:black;">' #;top:0px;width:0px;height:0px;border:solid 0px red;">'
            html_tbl += '<table id="' + name + '" border="2" bgColor="' + bgColor + '" class="ou" style="display:none;position:absolute;left:90px;">'
            html_tbl += '<tr bgColor="' + bgColor + '">'

            html_tbl += ' <th bgColor="' + fgColor + '" align="center">'
            html_tbl += ' <font color="' + bgColor + '">P</font>'

            html_tbl += ' <th bgColor="' + fgColor + '" align="center">'
            html_tbl += ' <font color="' + bgColor + '">O</font>'

            html_tbl += ' <th bgColor="' + fgColor + '" align="center">'
            html_tbl += ' <font color="' + bgColor + '">U</font></tr>'

            if "OVERUNDER" in tahmin:
                OU = tahmin["OVERUNDER"]
                for ou_giris in OU:
                    html_tbl += '<tr>'
                    html_tbl += '<td class="ou">' + ou_giris['Param'] + '</td>'
                    html_tbl += '<td class="ou">&nbsp;' + ou_giris['O'] + ' &nbsp;</td>'
                    html_tbl += '<td class="ou">' + ou_giris['U'] + '</td>'
                    html_tbl += '</tr>'

            html_tbl += '</table>'
            html_tbl += '</div></div>'
            print >>dosya_sonuc, html_tbl
            ####################### OU ##########################################

            home_tmp = home_current
            away_tmp = away_current
            print >>dosya_sonuc, '</a></div>' #event column
            alt += 1

        print >>dosya_sonuc, '<div class="clear"></div>'
        print >>dosya_sonuc, '</div>' #event-right
        print >>dosya_sonuc, '<div class="clear"></div>'
        print >>dosya_sonuc, '</div>'
        print >>dosya_sonuc, '<hr />'
        ana += 1

    print >>dosya_sonuc, "</div></body></html>"
    dosya_sonuc.close()

    #copyfmt='%Y%m%d-%H%M%S'
    #dosyaismi_yedek = dosyaismi_sonuc.split('.')[0] + '_' + str(datetime.now().strftime(copyfmt)) + '.htm'

    #copyfile(dosyaismi_sonuc, dosyaismi_yedek)
    #import ConfigParser
    #config = ConfigParser.ConfigParser()
    #filename = "konfig.txt"
    #config.read(filename)

    WebDokuman = config.get('Einstellungen', 'WebDokuman')
    if WebDokuman != '':
        print "www wird kopiert zu: " + config.get('Einstellungen', 'WebDokuman')
        copyfile(dosyaismi_sonuc, config.get('Einstellungen', 'WebDokuman'))

    print "Tabelle Erstellung wurde abgeschlossen"
    print "Vohandener Mode: " + config.get('Einstellungen', 'Mod')
    del config
# ############################################################################################################### #

def oufinder(arg01, arg02, arg03):

    config = ConfigParser.ConfigParser()
    filename = "konfig.txt"
    config.read(filename)
    print "Set OU..."
    config.set('Einstellungen', 'Mod', 'OU')
    #Das ist noetig um OU zu speichern
    fp = open(filename, 'w')
    config.write(fp)
    fp.close()

    started = datetime.now().replace(microsecond=0)
    print(started)

    fmt='%Y%m%d%H%M%S'
    #OUIslenecekSayfaAdet
    ou_islenecek_sayfa_adet = int(config.get('Einstellungen', 'OUIslenecekSayfaAdet'))
    sayfadaki_giris_adet = 20
    ou_genislik = ou_islenecek_sayfa_adet*sayfadaki_giris_adet

    ou_cekilecek_kategori_listesi = ["futbol", "tenis", "buz_hokeyi", "basket", "hentbol", "voleybol"]
    ou_cekilecek_kategori_listesi = ["football", "tennis", "ice-hockey", "basketball", "handball", "volleyball"]

    dosyaismi_json = "sportwette_over_under.json"

    dosya_dictlist = open(dosyaismi_json, 'r')

    try:
        templist = json.load(dosya_dictlist) # load-->object loads-->String
    except Exception:
        print "error: " + dosyaismi_json

    dosya_dictlist.close()

    # ---------------------------------------------------------------- #
    homepage="https://www.betbrain.com/"
    s = 0
    ou_cekilecek_kategorideki_maclar_listesi = []
    ou_cekilmeyecek_kategorideki_maclar_listesi = []
    # ################################################################ #
    print "Die Listegroesse der gesamten Kategorien: " + str(len(templist))

    #for index, giris in enumerate(templist):
    #    kategori = urlparse(giris['Link']).path.split('/')[1]
    #    if kategori in ou_cekilecek_kategori_listesi:
    #        s += 1
    #        ou_cekilecek_kategorideki_maclar_listesi.append(index)
    #    else:
    #        ou_cekilmeyecek_kategorideki_maclar_listesi.append(index)
    #dictlist = [i for j, i in enumerate(templist) if j in ou_cekilecek_kategorideki_maclar_listesi]
    #ousuz_diclist = [i for j, i in enumerate(templist) if j not in ou_cekilecek_kategorideki_maclar_listesi]
    #print "ou_cekilecek_kategorideki_maclar_liste_buyuklugu: " + str(len(dictlist))

    # ################################################################ #
    dictlist = templist

    #driver = webdriver.Firefox()
    
    # ################################################# #
    #firefox_gecko_list = []
    #for proc in psutil.process_iter():
    #    try:
    #        pinfo = proc.as_dict(attrs=['pid', 'name'])
    #    except psutil.NoSuchProcess:
    #        print "no such proc"
    #        pass
    #    else:
    #        if proc.name() == 'firefox.exe':
    #            if proc.pid not in firefox_desktop_list:
    #                firefox_gecko_list.append(proc.pid)                 
    # ################################################## #
    gecikme = int(config.get('Einstellungen', 'Gecikme'))

    elt_ou="//ul[@class='PopularBetTypes']//li//a//div//span[text() = 'Over/Under']"

    k=1
    girisler=[]

    buyukluk = len(dictlist)

    for giris in dictlist:
        if k <= ou_genislik:
            link=giris['Link']
            OverUnder = []
            try:
                driver.get(link)
                WebDriverWait(driver, gecikme).until(EC.presence_of_element_located((By.XPATH, elt_ou)))
            except TimeoutException:
                print str(k) + "/" + str(buyukluk) + " yuklenemedi: " + giris['Link']
            except NoSuchElementException:
                print "NoSuchElementException!"
            except Exception:
                print "No Marionette decoding possible!"
                break
            else:
                try:
                    driver.find_elements_by_xpath(elt_ou)[0].click() #  make one
                    # klik.click() # AND list index out of range error if elt_ou[1]
                except ElementNotVisibleException:
                    print "Kliklenemeyen O/U"
                except NoSuchElementException:
                    print "O/U eleman yok"
                else:
                    try:
                        divs = driver.find_elements_by_xpath("//div[@class='Collapsible OutcomeBox']") # 2017 01 11 Gives highest OU values. Need to get average values.
                        divs_adet = len(divs) # dIdriver.find_element_by_xpath("//div[@class='Collapsible OutcomeBox']").size
                    except NoSuchElementException: # Olesya Betina exception
                        print "NoSuchElement OU Divs"
                        break
                    except WebDriverException:
                        print"webdrv exception"
                        break
                    else:
                        print str(k) + "/" + str(buyukluk) + ": " + giris['Link'] # + " O/U adet: "
                        d=1
                        for div in divs:
                            elt_param="(//div[@class='Collapsible OutcomeBox CollapsibleActive'])["+str(d)+"]//span[@class='CollapsibleTitle']"
                            elt_bookies="(//div[@class='Collapsible OutcomeBox CollapsibleActive'])["+str(d)+"]//div[@class='BookiesHead'][1]//span//span[2]"
                            elt_o="(//div[@class='Collapsible OutcomeBox CollapsibleActive'])["+str(d)+"]//div[@class='AverageHighestContainer']//ul[1]//li[2]"
                            elt_u="(//div[@class='Collapsible OutcomeBox CollapsibleActive'])["+str(d)+"]//div[@class='AverageHighestContainer']//ul[1]//li[3]"
                            try:
                                div.click() #Collapsible OutcomeBox CollapsibleActive PARAM icin de, OVER UNDER icin de CollapsibleActive ibaresi gerekli.
                            except StaleElementReferenceException:
                                param = "Stale"
                                o = "Stale"
                                u = "Stale"
                                bookies = "Stale"
                                OU = {"Param":param,"O":o,"U":u}
                                print "Param: " + param + " O: " + o + " U: " + u + " Bookies: " + bookies
  
                            except WebDriverException:
                                param = "EltNotSeen"
                                o = "EltNotSeen"
                                u = "EltNotSeen"
                                OU = {"Param":param,"O":o,"U":u}
                                print "Param: " + param + " O: " + o + " U: " + u  # " Bookies: " + bookies
                            except Exception:
                                param = "TOTAL. Network timeout"
                                o = "TOTAL. Network timeout"
                                u = "TOTAL. Network timeout"
                                bookies = "TOTAL. Network timeout"
                                OU = {"Param":param,"O":o,"U":u}
                                print "Param: " + param + " O: " + o + " U: " + u + " Bookies: " + bookies
                            else:
                                try:
                                    param = div.find_element_by_xpath(elt_param).text
                                    bookies = div.find_element_by_xpath(elt_bookies).text
                                    o=div.find_element_by_xpath(elt_o).text
                                    u=div.find_element_by_xpath(elt_u).text
                                except NoSuchElementException:
                                    param = "NonE"
                                    o = "NonE"
                                    u = "NonE"
                                    bookies = "NonE"
                                    OU = {"Param":param,"O":o,"U":u}
                                    print "Param: " + param + " O: " + o + " U: " + u + " Bookies: " + bookies
                                    #break
                                except StaleElementReferenceException:
                                    param = "Stale"
                                    o = "Stale"
                                    u = "Stale"
                                    OU = {"Param":param,"O":o,"U":u}
                                    print "Param: " + param + " O: " + o + " U: " + u
                                except Exception:
                                    param = "TOTAL. Network timeout"
                                    o = "TOTAL. Network timeout"
                                    u = "TOTAL. Network timeout"
                                    bookies = "TOTAL. Network timeout"
                                    OU = {"Param":param,"O":o,"U":u}
                                    print "Param: " + param + " O: " + o + " U: " + u + " Bookies: " + bookies
                                else:
                                    OU = {"Param":param,"O":o,"U":u}
                                    print "Param: " + param + " O: " + o + " U: " + u + " Bookies: " + bookies
                            OverUnder.append(OU)
                            d += 1
                            if d > divs_adet:
                                break

            onceki_tahminler = giris["Odds"]
            sondaki_tahmin = giris["Odds"][-1]

            if "OVERUNDER" in sondaki_tahmin:
                tahmin = {"Date":sondaki_tahmin["Date"],"Home":sondaki_tahmin["Home"],"Away":sondaki_tahmin["Away"],"IsAverage":True}
                tahmin["OVERUNDER"]=OverUnder
                onceki_tahminler.append(tahmin)
                giris["Odds"] = onceki_tahminler
                girisler.append(giris)
                print "Mevcut OVERUNDER girisine eklenecek. " + giris["Link"]
            if not "OVERUNDER" in sondaki_tahmin:  #OVERUNDER girisi yoksa sondaki tahmini sil, OVERUNDER ekle ve sona ekle
                onceki_tahminler.remove(sondaki_tahmin)
                sondaki_tahmin["OVERUNDER"] = OverUnder
                onceki_tahminler.append(sondaki_tahmin)
                giris["Odds"] = onceki_tahminler
                girisler.append(giris)
                print "Yeni OVERUNDER girisi olusturulacak " + giris["Link"]
            k += 1
        else:
            girisler.append(giris)
            
    dosyaismi_json = "sportwette_over_under.json"
    dosya = open(dosyaismi_json, 'w')
    dosya.write(json.dumps(girisler))
    dosya.close()

    copyfmt='%Y%m%d-%H%M%S'
    dosyaismi_yedek = "json-yedek\\" + dosyaismi_json.split('.')[0] + '_' + str(datetime.now().strftime(copyfmt)) + '.' + dosyaismi_json.split('.')[1]
    copyfile(dosyaismi_json, dosyaismi_yedek)

    config.set('Einstellungen', 'Mod', 'HA')
    fp = open(filename, 'w')
    config.write(fp)
    fp.close()
    print "Der Mode nach der OU Bearbeitung: " + config.get('Einstellungen', 'Mod')

    ended = datetime.now().replace(microsecond=0)
    print "Gesamter Dauer: " + str(ended-started)

    #psutil.pids()
    #for proc in psutil.process_iter():
    #    try:
    #        pinfo = proc.as_dict(attrs=['pid', 'name'])
    #    except psutil.NoSuchProcess:
    #        print "no such proc"
    #        pass
    #    else:
    #        if proc.name() == 'geckodriver.exe':
    #            proc.kill()
    #        if proc.name() == 'firefox.exe':
    #            proc.kill()

    json2table(dosyaismi_json,"bet_sonuc.htm", 1)

    print config.get('Einstellungen', 'Bekleme') +  " Sekunden gewartet wird..."
    time.sleep(int(config.get('Einstellungen', 'Bekleme')))
    config.set('Einstellungen', 'SonOU', ended)

    fp = open(filename, 'w')
    config.write(fp)
    fp.close()
    del config
# ############################################################################################################ #
def halister():
# home away
    fmt='%d.%m.%Y %H:%M'
    yerel_zd = timezone('Europe/Istanbul')

    config = ConfigParser.ConfigParser()
    filename = "konfig.txt"
    config.read(filename)

    started = datetime.now().replace(microsecond=0)
    print "HA basladi: " + started.strftime(fmt)

    dosyaismi_json = "sportwette_over_under.json"
    #dosyaismi_json = "skeleton.json"

    cekilecek_sayfa_adet = int(config.get('Einstellungen', 'HA_seite_steuck'))
    sayfadaki_giris_adet = 20
    ha_genislik = cekilecek_sayfa_adet*sayfadaki_giris_adet

    def utc_to_local(utc_dt):
        local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(yerel_zd)
        return yerel_zd.normalize(local_dt) # .normalize might be unnecessary

    mod = config.get('Einstellungen', 'Mod')
    if mod == "OU":
        print "ou verileri islenecek..."
        #execfile('beta-11-ou-finder.py')
        oufinder(dosyaismi_json,"bet_sonuc.htm",0)
        #config.set('Einstellungen', 'Mod', 'HA')
        time.sleep(1)
    else:
        print "ha verileri islenecek..."

    dosya = open(dosyaismi_json, 'r')
    hamliste = json.load(dosya)
    dosya.close()

    lengthbefore = str( len(hamliste))
    print "toplam mac sayisi: " + lengthbefore

    s = 0
    suresi_gecenler_endeksleri = []

    for index, giris in enumerate(hamliste):
        StartDate = datetime.strptime(giris["StartDate"], fmt)
        print "StartDate: " + StartDate.strftime(fmt) + " Date now: " + datetime.now().strftime(fmt)
        if datetime.now() > StartDate:
            print "to be removed: " + " @" + str(giris["StartDate"])
            s += 1
            #print(index)
            suresi_gecenler_endeksleri.append(index)
            #dictlist.remove(giris)
    print "suresi gecen / silinecek mac sayisi: " + str(s)
    #print(suresi_gecenler_endeksleri)

    dictlist = [i for j, i in enumerate(hamliste) if j not in suresi_gecenler_endeksleri]

    print "kalan mac sayisi: " + str(len(dictlist))
    #print "length before: " + lengthbefore + " later: " + str( len(dictlist))

    dosya = open(dosyaismi_json, 'w')
    dosya.write(json.dumps(dictlist))
    dosya.close()

    driver.get("http://betbrain.com/next-matches/")
 
    for sayfa in range(0, cekilecek_sayfa_adet):
        try:
            element = WebDriverWait(driver, 1).until(
            EC.presence_of_element_located((By.XPATH, "//button[@class='Button SportsBoxAll LoadMore']")))
        except NoSuchElementException:
            print "NoSuchElement Exception"
            continue
        except TimeoutException:
            print"Timeout Exception (muhtemel sayfa sonu)"
            break
        else:
            element.click()
            print "Cekilen sayfa: " + str(sayfa)

    idlist = []

    for itm in dictlist:
        tmp=itm['Id']
        idlist.append(tmp)

    lis = driver.find_elements_by_xpath("""//li[@class="Match"]""")

    buyukluk = 0
    for li in lis:
        buyukluk += 1

    girisler= [] # nicht mehr zutreffend! 

    k=1
    timeout_error_count = 0
    
    for li in lis:
        try: # SOCKET ERROR (zum vermeiden)
            a = driver.find_element_by_xpath("//li[@class='Match']["+str(k)+"]//a[@class='MatchTitleLink']")
            id = li.get_attribute("data-reactid")
            tarih = driver.find_element_by_xpath("//li[@class='Match']["+str(k)+"]//span[@class='DateTime']/time").text
            link = a.get_attribute("href")
        except:
            timeout_error_count += 1
            break
        baslik_liste = a.text.split(u'\u2014')
        baslik_metni_duz = baslik_liste[0] + " - " + baslik_liste[1]
        #str(baslik_metni_duz).encode('utf-8')
        #baslik_metni_duz.encode(sys.stdout.encoding, errors='replace')
        try:
            span_home= driver.find_element_by_xpath("(//li[@class='Match']["+str(k)+"]//ol[@class='BetList']//li[@class='Bet'])[1]//span[@class='AverageOdds']//span[2]").text
        except Exception:
            span_home="NA"
        try:
            span_away= driver.find_element_by_xpath("(//li[@class='Match']["+str(k)+"]//ol[@class='BetList']//li[@class='Bet'])[3]//span[@class='AverageOdds']//span[2]").text
        except Exception:
            span_away="NA"
            
        tahmin_tar = datetime.now().strftime(fmt)
        tahmin = {"Date":tahmin_tar,"Home":span_home,"Away":span_away,"IsAverage":True}

        baslangic_tar_temp = datetime.strptime(tarih, '%d/%m/%Y %H:%M')
        baslangic_tar = utc_to_local(baslangic_tar_temp).strftime(fmt)

        try:
            print str(k) + "/" + str(buyukluk) + ": " + baslik_metni_duz + " Datum: " + baslangic_tar + " H: " + span_home + " A: " + span_away
        except UnicodeEncodeError:
            print str(k) + "/" + str(buyukluk) + ": bozuk karakter"

        cleanid=str(id).split("$")[1]
        if str(cleanid) in idlist:
            bulunan_giris_temp = filter(lambda giris: giris['Id'] == cleanid, dictlist) # filter (function, sequence) Python sequence, Lisp list, Javascript Array
            bulunan_giris = bulunan_giris_temp[0] # because its a sequence the first zeroth element is taken

            tahminler = bulunan_giris['Odds'] # here it will be limited to [limit] number of  entries!

            ############### TRIMMING ###################
            tahminler_buyukluk = len(tahminler)
            if tahminler_buyukluk > limit:
                silinecek_tahmin_sayisi = tahminler_buyukluk - limit
                print str(k) + "/" + str(buyukluk) + " silinecek tahmin sayisi: " + str(silinecek_tahmin_sayisi)
                del tahminler[0:silinecek_tahmin_sayisi]
            ########### ################################
            tahminler.append(tahmin)
            bulunan_giris["Odds"] = tahminler
            print "Bestehender Eintrag: "
        else:
            tahminler= []
            tahminler.append(tahmin)
            giris = {"StartDate":baslangic_tar,"Odds":tahminler,"Link":link,"Id":cleanid,"Name":baslik_metni_duz}

            #girisler.append(giris)
            dictlist.append(giris)
            print "Neuer Eintrag: "
        k += 1
        
    #driver.close()
    #prockiller(firefox_gecko_list)
    
    # bogo bubble #
    siralansin_mi=1
    if siralansin_mi == 1:
        print "Spielzeiten werden angeordnet. Warte..."
        sirali_dictlist, cmp_sayisi, degisim_sayisi = bogo_bubble(dictlist)

    if not timeout_error_count > buyukluk-20:
        
        dosyaismi_json = "sportwette_over_under.json"
        print "saving " + dosyaismi_json
        dosya = open(dosyaismi_json, 'w')
        dosya.write(json.dumps(sirali_dictlist))
        dosya.close()
        json2table(dosyaismi_json,"bet_sonuc.htm",1)
        oufinder(dosyaismi_json,"bet_sonuc.htm",0)
        print "1 sn bekle... "
        time.sleep(1)
    else:
        print "Cok fazla timeout hatasi dolayisiyla json güncellenip kaydedilmedi"
        
    ended = datetime.now().replace(microsecond=0)
    print "toplam sure: " + str(ended-started)

    del config


# #################################################################################################### #
# RUNNER START SECTION
# ################### #

firefox_desktop_list=[]
firefox_gecko_list=[]

psutil.pids()
 
for proc in psutil.process_iter():
    try:
        pinfo = proc.as_dict(attrs=['pid', 'name'])
    except psutil.NoSuchProcess:
        print "no such proc"
        pass
    else:
        if proc.name() == 'firefox.exe':
            firefox_desktop_list.append(proc.pid)

config = ConfigParser.ConfigParser()
filename = "konfig.txt"
config.read(filename)

limit = int(config.get('Einstellungen', 'limit'))   #tahmin
print "limit: " + str(limit)
config.set('Einstellungen', 'SonOU', datetime.now().replace(microsecond=0))

driver = webdriver.Firefox(executable_path='C:\Users\win7\progs\geckodriver.exe')

# driver = webdriver.Firefox()
driver.implicitly_wait(int(config.get('Einstellungen','NetworkTimeout')))
driver.maximize_window()

#firefox_gecko_list = []
for proc in psutil.process_iter():
    try:
        pinfo = proc.as_dict(attrs=['pid', 'name'])
    except psutil.NoSuchProcess:
        print "no such proc"
        pass
    else:
        if proc.name() == 'firefox.exe' and proc.pid not in firefox_desktop_list:
            firefox_gecko_list.append(proc.pid)

#driver.manage().window().setPosition(new Point(-2000, 0))

import signal
def signal_handler(signal, frame):
        print('Ctrl+C gedrueckt, zumachen.')
        time.sleep(1)

        firefox_gecko_str=""

        for entry in firefox_gecko_list:
                firefox_gecko_str += "-" + str(entry)

        print "Die zu loeschende gecko pid: " + firefox_gecko_str
        time.sleep(1)
        prockiller(firefox_gecko_list)
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

#print('Press Ctrl+C')
#signal.pause()

fp = open(filename, 'w')
config.write(fp)
fp.close()
del config

print "Programi durdurmak icin Ctrl+C basin."
while 1==1:
    halister()
